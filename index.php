<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Rock Paper Scissors</title>
        <link rel="stylesheet" href="css/vue.css">
    </head>
    <body>
        <h1 id="title">{{ title }}</h1>
        <br />

        <?php include ("partials/footer.php");?>

        <script type="text/javascript" src="js/vue.min.js"></script>
        <script type="text/javascript" src="js/index.js"></script>
    </body>
</html>
