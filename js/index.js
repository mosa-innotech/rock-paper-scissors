var d = new Date();
var currentYear = d.getFullYear();
var title = "Rock Paper Scissors";

new Vue({
  el: '#title',
  data: {
    title: title
  }
})

new Vue({
  el: '#footer',
  data: {
    text: '\u00A9 ' + currentYear + " "+ title
  }
})
